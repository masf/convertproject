﻿namespace ConvertProject
{
    partial class convertProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFolderTo = new System.Windows.Forms.Button();
            this.txtFolderTo = new System.Windows.Forms.TextBox();
            this.btnFolderFrom = new System.Windows.Forms.Button();
            this.txtFolderFrom = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFolderRemove = new System.Windows.Forms.TextBox();
            this.txtExtensionFileRemove = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNomeNovoProjeto = new System.Windows.Forms.TextBox();
            this.txtNomeDoProjeto = new System.Windows.Forms.TextBox();
            this.btnConverter = new System.Windows.Forms.Button();
            this.fbd = new System.Windows.Forms.FolderBrowserDialog();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnFolderTo);
            this.groupBox1.Controls.Add(this.txtFolderTo);
            this.groupBox1.Controls.Add(this.btnFolderFrom);
            this.groupBox1.Controls.Add(this.txtFolderFrom);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 78);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pasta do projeto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pasta de Destino";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Projeto Principal";
            // 
            // btnFolderTo
            // 
            this.btnFolderTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFolderTo.Location = new System.Drawing.Point(357, 44);
            this.btnFolderTo.Name = "btnFolderTo";
            this.btnFolderTo.Size = new System.Drawing.Size(34, 23);
            this.btnFolderTo.TabIndex = 3;
            this.btnFolderTo.Text = "...";
            this.btnFolderTo.UseVisualStyleBackColor = true;
            this.btnFolderTo.Click += new System.EventHandler(this.btnFolderTo_Click);
            // 
            // txtFolderTo
            // 
            this.txtFolderTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFolderTo.Location = new System.Drawing.Point(96, 46);
            this.txtFolderTo.Name = "txtFolderTo";
            this.txtFolderTo.Size = new System.Drawing.Size(255, 20);
            this.txtFolderTo.TabIndex = 2;
            // 
            // btnFolderFrom
            // 
            this.btnFolderFrom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFolderFrom.Location = new System.Drawing.Point(357, 18);
            this.btnFolderFrom.Name = "btnFolderFrom";
            this.btnFolderFrom.Size = new System.Drawing.Size(34, 23);
            this.btnFolderFrom.TabIndex = 1;
            this.btnFolderFrom.Text = "...";
            this.btnFolderFrom.UseVisualStyleBackColor = true;
            this.btnFolderFrom.Click += new System.EventHandler(this.btnFolderFrom_Click);
            // 
            // txtFolderFrom
            // 
            this.txtFolderFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFolderFrom.Location = new System.Drawing.Point(96, 20);
            this.txtFolderFrom.Name = "txtFolderFrom";
            this.txtFolderFrom.Size = new System.Drawing.Size(255, 20);
            this.txtFolderFrom.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtFolderRemove);
            this.groupBox2.Controls.Add(this.txtExtensionFileRemove);
            this.groupBox2.Location = new System.Drawing.Point(13, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(396, 90);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Configuração";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Pastas Excluidas";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Extensões Excluidas";
            // 
            // txtFolderRemove
            // 
            this.txtFolderRemove.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFolderRemove.Location = new System.Drawing.Point(109, 45);
            this.txtFolderRemove.Name = "txtFolderRemove";
            this.txtFolderRemove.Size = new System.Drawing.Size(281, 20);
            this.txtFolderRemove.TabIndex = 5;
            this.txtFolderRemove.Text = "bin;obj;debug;release;.git;.svn;_ReSharper;.vs";
            // 
            // txtExtensionFileRemove
            // 
            this.txtExtensionFileRemove.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtensionFileRemove.Location = new System.Drawing.Point(109, 19);
            this.txtExtensionFileRemove.Name = "txtExtensionFileRemove";
            this.txtExtensionFileRemove.Size = new System.Drawing.Size(281, 20);
            this.txtExtensionFileRemove.TabIndex = 4;
            this.txtExtensionFileRemove.Text = ".cache;.mst;.msm;.gitignore;.idx;.pack;.user;.resharper;.suo;.vsscc;desktop.ini;t" +
    "humbs.db";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtNomeNovoProjeto);
            this.groupBox3.Controls.Add(this.txtNomeDoProjeto);
            this.groupBox3.Location = new System.Drawing.Point(13, 96);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(396, 90);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Configuração";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Nome da Novo Projeto";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Nome do Projeto Existente";
            // 
            // txtNomeNovoProjeto
            // 
            this.txtNomeNovoProjeto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNomeNovoProjeto.Location = new System.Drawing.Point(154, 45);
            this.txtNomeNovoProjeto.Name = "txtNomeNovoProjeto";
            this.txtNomeNovoProjeto.Size = new System.Drawing.Size(236, 20);
            this.txtNomeNovoProjeto.TabIndex = 5;
            // 
            // txtNomeDoProjeto
            // 
            this.txtNomeDoProjeto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNomeDoProjeto.Location = new System.Drawing.Point(154, 19);
            this.txtNomeDoProjeto.Name = "txtNomeDoProjeto";
            this.txtNomeDoProjeto.Size = new System.Drawing.Size(236, 20);
            this.txtNomeDoProjeto.TabIndex = 4;
            // 
            // btnConverter
            // 
            this.btnConverter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConverter.Location = new System.Drawing.Point(329, 285);
            this.btnConverter.Name = "btnConverter";
            this.btnConverter.Size = new System.Drawing.Size(80, 23);
            this.btnConverter.TabIndex = 6;
            this.btnConverter.Text = "Converter";
            this.btnConverter.UseVisualStyleBackColor = true;
            this.btnConverter.Click += new System.EventHandler(this.btnConverter_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(13, 284);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(396, 23);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 9;
            this.progressBar.Visible = false;
            // 
            // convertProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 325);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.btnConverter);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "convertProject";
            this.Text = "Converter Projeto";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFolderTo;
        private System.Windows.Forms.TextBox txtFolderTo;
        private System.Windows.Forms.Button btnFolderFrom;
        private System.Windows.Forms.TextBox txtFolderFrom;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFolderRemove;
        private System.Windows.Forms.TextBox txtExtensionFileRemove;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNomeNovoProjeto;
        private System.Windows.Forms.TextBox txtNomeDoProjeto;
        private System.Windows.Forms.Button btnConverter;
        private System.Windows.Forms.FolderBrowserDialog fbd;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}

