﻿using ConvertProject.Model;
using ConvertProject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ConvertProject.Services
{
    public class ProjectPath
    {
        private readonly String _pathBase;
        private readonly String _pathDest;
        private readonly int _totalFiles;
        private int _indexProgress = 0;

        private String _filterFilesExclusion;
        private String _filterPahtExclusion;


       public event EventHandler OnProgressing;


        public ProjectPath(String pathBase, String pathDest)
        {
            _pathBase = pathBase;
            _pathDest = pathDest;
            _totalFiles = Directory.GetFiles(pathBase,"*",SearchOption.AllDirectories).Count();
            _indexProgress = 0;
        }

        public void setFilterFilesExclusion(String filter)
        {
            _filterFilesExclusion = filter.ToUpper();
        }

        public void setFilterPathExclusion(String filter)
        {
            _filterPahtExclusion = filter.ToUpper();
        }

        public void Converter(String name, String toName, String path = "")
        {
            PathContent pc = GetDir(String.IsNullOrEmpty(path) ? _pathBase + "//" : path);

            foreach (var item in pc.Directories)
            {
                Directory.CreateDirectory(item.FullName.Replace(_pathBase, _pathDest).Replace(name, toName));
                //Directory.CreateDirectory(item.FullName.Replace(_pathBase, _pathDest) + "\\" + toName);
                Converter(name, toName, item.FullName);
            }

            foreach (var item in pc.Files)
            {
                Thread.Sleep(1);
                String fileText;
                using (FileStream fs = new FileStream(item.FullName, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader writer = new StreamReader(fs))
                    {
                        fileText = writer.ReadToEnd();
                    }
                }

                using (FileStream fs = new FileStream(item.FullName.Replace(_pathBase, _pathDest).Replace(name, toName), FileMode.Create, FileAccess.Write))
                {
                    using (StreamWriter writer = new StreamWriter(fs))
                    {
                        var retval = fileText.Replace(name, toName);
                        writer.Write(retval);
                    }
                }
                _indexProgress++;
                OnProgressingEvent(new ProgressingEventArgs()
                {
                    IndexFileProgressing = _indexProgress,
                    TotalFiles = _totalFiles,
                    ProgressProcent = Convert.ToInt32(((_indexProgress * 100) / _totalFiles))
                });
            }
        }

        protected virtual void OnProgressingEvent(ProgressingEventArgs e)
        {
            EventHandler handler = OnProgressing;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        private PathContent GetDir(String path)
        {
            PathContent pc = new PathContent();
            DirectoryInfo dir = new DirectoryInfo(path);

            List<DirectoryInfo> dirA = pc.Directories = new List<DirectoryInfo>();
            DirectoryInfo[] ss = dir.GetDirectories();
            foreach (DirectoryInfo item in ss)
            {
                if (_filterPahtExclusion.Contains(item.Name.ToUpper()))
                    continue;
                dirA.Add(item);
            }
            pc.Directories = dirA;

            List<FileInfo> FileA = pc.Files = new List<FileInfo>();
            FileInfo[] ff = dir.GetFiles();
            foreach (FileInfo item in ff)
            {
                if (_filterFilesExclusion.Contains(item.Name.ToUpper()))
                    continue;
                FileA.Add(item);
            }

            pc.Directories = dirA;
            pc.Files = FileA;

            return pc;
        }
    }
}
