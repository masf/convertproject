﻿using ConvertProject.Model;
using ConvertProject.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertProject
{
    public partial class convertProject : Form
    {
        public convertProject()
        {
            InitializeComponent();
        }

        private void btnFolderFrom_Click(object sender, EventArgs e)
        {
            fbd.ShowDialog();
            txtFolderFrom.Text = fbd.SelectedPath;
        }

        private void btnFolderTo_Click(object sender, EventArgs e)
        {
            fbd.ShowDialog();
            txtFolderTo.Text = fbd.SelectedPath;
        }

        private void btnConverter_Click(object sender, EventArgs e)
        {
            try
            {
                progressBar.Visible = true;
                Validate();

                ProjectPath pp = new ProjectPath(txtFolderFrom.Text, txtFolderTo.Text);
                pp.OnProgressing += Pp_OnProgressing;
                pp.setFilterFilesExclusion(txtExtensionFileRemove.Text);
                pp.setFilterPathExclusion(txtFolderRemove.Text);
                pp.Converter(txtNomeDoProjeto.Text, txtNomeNovoProjeto.Text);
                MessageBox.Show("Conversão realizada com sucesso!");
            }
            catch (Exception ex)
            {                
                    MessageBox.Show(ex.Message);
                    return;
            }
            progressBar.Visible = false;
        }

        private void Pp_OnProgressing(object sender, EventArgs e)
        {
            if (e is ProgressingEventArgs)
            {
                progressBar.Value = ((ProgressingEventArgs)e).ProgressProcent;
            }
        }

        private void Validate()
        {
            if (!Directory.Exists(txtFolderFrom.Text))
            {
                throw new Exception("A pasta do projeto principal é obrigatório");
            }
            if (!Directory.Exists(txtFolderTo.Text))
            {
                throw new Exception("A pasta do novo projeto, aonde o projeto será copiado, é obrigatório");
            }
            if (String.IsNullOrEmpty(txtNomeDoProjeto.Text) )
            {
                throw new Exception("O nome do projeto é obrigatório");
            }
            if (String.IsNullOrEmpty(txtNomeNovoProjeto.Text))
            {
                throw new Exception("O nome do novo projeto é obrigatório");
            }
        }
    }
}
