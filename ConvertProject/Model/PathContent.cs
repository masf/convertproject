﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertProject.Models
{
   public class PathContent
    {
        public List<DirectoryInfo> Directories { get; set; }
        public List<FileInfo> Files { get; set; } 
    }
}
