﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertProject.Model
{
    public class ProgressingEventArgs : EventArgs
    {
        public int TotalFiles { get; set; }
        public int IndexFileProgressing { get; set; }
        public int ProgressProcent { get; set; }
    }
}
